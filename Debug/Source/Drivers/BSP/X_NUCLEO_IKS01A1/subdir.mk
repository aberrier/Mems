################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1.c \
../Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_hum_temp.c \
../Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_imu_6axes.c \
../Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_magneto.c \
../Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_pressure.c 

OBJS += \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1.o \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_hum_temp.o \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_imu_6axes.o \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_magneto.o \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_pressure.o 

C_DEPS += \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1.d \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_hum_temp.d \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_imu_6axes.d \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_magneto.d \
./Source/Drivers/BSP/X_NUCLEO_IKS01A1/x_nucleo_iks01a1_pressure.d 


# Each subdirectory must supply rules for building sources it contributes
Source/Drivers/BSP/X_NUCLEO_IKS01A1/%.o: ../Source/Drivers/BSP/X_NUCLEO_IKS01A1/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM32F401xE -DUSE_HAL_DRIVER -DUSE_STM32F4XX_NUCLEO -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DDEBUG_SEMIHOSTING_PRINTF -I"/home/ece/workspace/Mems/Source/App/Button" -I"/home/ece/workspace/Mems/Source/App/Main" -I"/home/ece/workspace/Mems/Source/App/MEMS" -I"/home/ece/workspace/Mems/Source/App/Motion" -I"/home/ece/workspace/Mems/Source/App/Time" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/Common" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/hts221" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lis3mdl" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lps25h" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lps25hb" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lsm6ds0" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lsm6ds3" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/STM32F4xx-Nucleo" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/X_NUCLEO_IKS01A1" -I"/home/ece/workspace/Mems/Source/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/home/ece/workspace/Mems/Source/Drivers/CMSIS/Include" -I"/home/ece/workspace/Mems/Source/Drivers/CMSIS/Lib" -I"/home/ece/workspace/Mems/Source/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/home/ece/workspace/Mems/Source/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/home/ece/workspace/Mems/Source/Drivers/Utils/Trace" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


