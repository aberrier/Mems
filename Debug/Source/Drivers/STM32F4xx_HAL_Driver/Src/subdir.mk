################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_can.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cec.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_crc.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma2d.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_eth.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hcd.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_irda.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_iwdg.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nand.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nor.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pccard.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_qspi.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sd.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_smartcard.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spdifrx.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sram.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_usart.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_wwdg.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fsmc.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_sdmmc.c \
../Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c 

OBJS += \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_can.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cec.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_crc.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma2d.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_eth.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hcd.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_irda.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_iwdg.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nand.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nor.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pccard.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_qspi.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sd.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_smartcard.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spdifrx.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sram.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_usart.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_wwdg.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fsmc.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_sdmmc.o \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.o 

C_DEPS += \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_can.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cec.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_crc.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma2d.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_eth.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hcd.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_irda.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_iwdg.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nand.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nor.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pccard.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_qspi.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sd.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_smartcard.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spdifrx.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sram.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_usart.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_wwdg.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fsmc.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_sdmmc.d \
./Source/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.d 


# Each subdirectory must supply rules for building sources it contributes
Source/Drivers/STM32F4xx_HAL_Driver/Src/%.o: ../Source/Drivers/STM32F4xx_HAL_Driver/Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSTM32F401xE -DUSE_HAL_DRIVER -DUSE_STM32F4XX_NUCLEO -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DDEBUG_SEMIHOSTING_PRINTF -I"/home/ece/workspace/Mems/Source/App/Button" -I"/home/ece/workspace/Mems/Source/App/Main" -I"/home/ece/workspace/Mems/Source/App/MEMS" -I"/home/ece/workspace/Mems/Source/App/Motion" -I"/home/ece/workspace/Mems/Source/App/Time" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/Common" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/hts221" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lis3mdl" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lps25h" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lps25hb" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lsm6ds0" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/Components/lsm6ds3" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/STM32F4xx-Nucleo" -I"/home/ece/workspace/Mems/Source/Drivers/BSP/X_NUCLEO_IKS01A1" -I"/home/ece/workspace/Mems/Source/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/home/ece/workspace/Mems/Source/Drivers/CMSIS/Include" -I"/home/ece/workspace/Mems/Source/Drivers/CMSIS/Lib" -I"/home/ece/workspace/Mems/Source/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/home/ece/workspace/Mems/Source/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/home/ece/workspace/Mems/Source/Drivers/Utils/Trace" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


